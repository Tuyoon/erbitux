//
//  AppDelegate.m
//  Hello
//
//  Created by Mac on 11.06.13.
//  Copyright (c) 2013 Mac. All rights reserved.
//

#import "AppDelegate.h"
#import "DataManager.h"

@implementation AppDelegate

@synthesize window = _window;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [DataManager sharedManager];
    return YES;
}

@end
