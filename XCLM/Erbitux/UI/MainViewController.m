//
//  MainViewController.m
//  XCLM
//
//  Created by Mac on 21.01.17.
//  Copyright © 2017 Mac. All rights reserved.
//

#import "MainViewController.h"
#import "PresentationViewController.h"
#import "LoadingView.h"
#import "User.h"
#import "Loader.h"

typedef enum : NSInteger {
    LoadingStateAuthorization,
    LoadingStateUpdate,
} LoadingState;

@interface MainViewController ()<ConnectDelegate, SynchronizationDelegate, PresentationDelegate>

@property (strong, nonatomic) Presentation* presentation;

@end

@implementation MainViewController {
    LoadingState _state;
    Connect* _connect;
    __weak IBOutlet UIProgressView *_progressView;
    
    __weak IBOutlet UIView *_errorView;
    __weak IBOutlet UIButton *_retryButton;
    __weak IBOutlet UILabel *_errorLabel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _connect = [[Connect alloc] init];
    _connect.delegate = self;
    self.navigationController.navigationBar.hidden = YES;
    _progressView.hidden = YES;
    [self hideError];
    [self authorize];
}

#pragma mark - Properties

- (void)setPresentation:(Presentation *)presentation{
    _presentation = presentation;
    _presentation.delegate = self;
    _presentation.connectDelegate = self;
    _presentation.synchronizationDelegate = self;
}

#pragma mark - Actions

- (IBAction)retryButtonPressed:(UIButton *)sender {
    [self hideError];
    switch (_state) {
        case LoadingStateAuthorization:
            [self authorize];
            break;
        case LoadingStateUpdate:
            [self update];
            break;
    }
}

#pragma mark - Private UI

- (void)showError:(NSString *)error {
    _errorView.hidden = NO;
    _errorLabel.text = error;
}

- (void)hideError {
    _errorView.hidden = YES;
}

#pragma mark - Private

- (void)authorize {
    [_connect login:@"xor@bmax.ru" password:@"12345"];
}

- (void)loadAndUpdate:(NSArray *)presentationIds {
    Presentation *presentation;
    
    for (NSDictionary *presentationDictionary in presentationIds) {
        if ([presentationDictionary[@"sysname"] isEqualToString:@"Erbitux"]) {
            presentation = [[Loader sharedInstance] load:@[presentationDictionary]].firstObject;
            [[Loader sharedInstance] save:@[presentation]];
            break;
        }
    }    
    
    if (presentation == nil) {
        [self showError:@"Ошибка обработки данных, обратитесь к администратору"];
        return;
    }
    
    self.presentation = presentation;
    [self update];
}

- (void)update {
    if([self.presentation needUpdate]) {
        _state = LoadingStateUpdate;
        if(![self.presentation update]) {
            [self showPresentation];
        }
    }
    else {
        [self showPresentation];
    }
}

- (void)updateDownloadProgress:(CGFloat)progress{
    _progressView.hidden = NO;
    _progressView.progress = progress;
}

- (void)showPresentation {
    PresentationViewController* controller = [[self storyboard] instantiateViewControllerWithIdentifier:@"PresentationViewController"];
    [controller setPresentation:self.presentation];
    [self.navigationController setViewControllers:@[controller] animated:NO];
}

#pragma mark - ConnectDelegate

- (void)connectLoginComplete:(LoginResponse *)loginResponse{
    User* user = [[User alloc] initWithLoginResponse:loginResponse];
    [user save];
    [self loadAndUpdate:loginResponse.presentationsIds];
}

- (void)connectLoginFailed:(NSString *)description {
    [self showError:[NSString stringWithFormat:@"Ошибка авторизации: %@", description]];
}

#pragma mark PresentationDelegate

- (void)presentationStartUpdate:(Presentation *)presentation{
    [self updateDownloadProgress:0];
    [_progressView setHidden:NO];
}

#pragma mark ConnectDelegat

-(void)updatePresentationComplete:(Presentation *)presentation{
    [self showPresentation];
}

-(void)updatePresentationFailed:(NSString *)description{
    [self showError:@"Ошибка обновления презентации"];
}

#pragma mark SynchronizationDelegate

- (void)synchronizationPartComplete:(NSString *)version rootPath:(NSString *)rootPath{
    
}

- (void)synchronizationComplete:(NSString *)rootPath{
    [self showPresentation];
}

- (void)synchronizationFailed:(NSString *)description{
    [self showError:@"Ошибка синхронизации презентации"];
}

- (void)synchronizationProgressChanged:(CGFloat)progress{
    [self updateDownloadProgress:progress];
}

@end
