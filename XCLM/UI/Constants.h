//
//  Constants.h
//  XCLM
//
//  Created by Mac on 31.10.15.
//  Copyright © 2015 Mac. All rights reserved.
//

// API URLs
FOUNDATION_EXPORT NSString *const kServer,
*const kLoginServer,
*const kPresentationsServer,
*const kVersionCheckServer,
*const kApplicationServer,
*const kDataServer,

// messages
*const kNeedInternet;

FOUNDATION_EXPORT CGFloat const kTimeInterval;