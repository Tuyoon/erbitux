//
//  Constants.m
//  XCLM
//
//  Created by Mac on 31.10.15.
//  Copyright © 2015 Mac. All rights reserved.
//

#import "Constants.h"

// API URLs

#if ERBITUX
#define BASE_URL @"http://merck.xclm.ru"
#elif ERBITUX_KZ
#define BASE_URL @"http://merck-kz.xpractice.ru"
#endif

#define BASE_FOLDER @"/data"

#ifdef BASE_URL
NSString* const kServer = BASE_URL,
*const kLoginServer = BASE_URL BASE_FOLDER @"/login/?login=%@&password=%@&appversion=%@",
*const kPresentationsServer = BASE_URL BASE_FOLDER @"/ziplist",
*const kVersionCheckServer = BASE_URL BASE_FOLDER @"/CLM/update",
*const kApplicationServer = BASE_URL @"/CLM",
*const kDataServer = BASE_URL BASE_FOLDER @"/send";
#endif

NSString *const kNeedInternet = @"Проверьте подключение к интернету";

CGFloat const kTimeInterval = 300;
