//
//  ViewController.m
//  Hello
//
//  Created by Mac on 11.06.13.
//  Copyright (c) 2013 Mac. All rights reserved.
//

#import "PresentationViewController.h"
#import "Connect.h"
#import <QuartzCore/QuartzCore.h>
#import "WebViewDelegate.h"
#import "User.h"
#import "DataManager.h"

#define MENU_BUTTON_LEFT 10
#define MENU_BUTTON_TOP 5
#define MENU_BUTTON_SPACING 8
#define MENU_SLIDE_YPOSITION 550
#define MENU_EMPTY_SLIDE_YPOSITION 700
#define HOME_BUTTON_WIDTH 57
#define MENU_BUTTON_HEIGHT 47
#define MENU_BUTTON_FONT_SIZE 13
#define SLIDE_WIDTH 204
#define SLIDE_HEIGHT 142
#define SLIDE_SPACING 10

@interface PresentationViewController ()<UIDocumentInteractionControllerDelegate>{
    Presentation* _presentation;
    CGSize screenSize;
    UIDocumentInteractionController* _documentInteractionController;
    NSString * _presentationFolderName;
    UITapGestureRecognizer *_unlockGestureRecognizer;
    NSInteger _currentSlideButtonIndex;
    NSMutableArray * _slideNames;
}

@property WebViewDelegate* webViewDelegate;

@end

@implementation PresentationViewController

-(BOOL) prefersStatusBarHidden{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [UIApplication sharedApplication].idleTimerDisabled = NO;
    NSString* documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    _presentationFolderName = [documentsDirectory stringByAppendingPathComponent:[_presentation.path lastPathComponent]];
    
    [[DataManager sharedManager] startSessionForPresentation:_presentation];
    [self prepareWebView];
    [self setupSlideMenu];
    [self loadPresentation];
    [self addDoubleTapRecognizer];
    [self addApplicationNotifications];
    screenSize = [[UIScreen mainScreen] bounds].size;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

- (void)viewWillUnload {
    [super viewWillUnload];
    [[DataManager sharedManager] stopCurrentSession];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //    return FALSE;
    return UIInterfaceOrientationIsLandscape( interfaceOrientation );
}
- (void)didReceiveMemoryWarning
{
	[[NSURLCache sharedURLCache] removeAllCachedResponses];
	[super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark Public methods

- (void)setPresentation:(Presentation *)presentation{
    _presentation = presentation;
}

#pragma mark -
#pragma mark Private Methods

- (void)addApplicationNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
}

- (void)applicationDidBecomeActive:(UIApplication *)application{
    [_webView stringByEvaluatingJavaScriptFromString:@"xclm.app.expand()"];
}

- (void)applicationDidEnterBackground:(UIApplication *)application{
    [_webView stringByEvaluatingJavaScriptFromString:@"xclm.app.collapse()"];
}

- (void)applicationWillTerminateNotification:(UIApplication *)application{
    [_webView stringByEvaluatingJavaScriptFromString:@"xclm.app.close()"];
}

- (void)prepareWebView{
    self.webViewDelegate = [[WebViewDelegate alloc] initWithWebView:self.webView withWebViewInterface:self];
    _webView.scrollView.pagingEnabled = NO;
    _webView.scrollView.alwaysBounceHorizontal = NO;
    _webView.scrollView.alwaysBounceVertical = NO;
    _webView.scrollView.bounces = NO;
    _webView.mediaPlaybackRequiresUserAction = NO;
    _webView.scrollView.scrollEnabled = NO;
}

- (void) loadPresentation{
    
    NSString* indexPath = [_presentationFolderName stringByAppendingPathComponent:@"index.html"];
    NSFileManager* fileManager = [NSFileManager defaultManager];
    [self loadURL:indexPath];
}

- (void)toExit{
    exit(0);
}

- (void)addDoubleTapRecognizer{
    UITapGestureRecognizer* recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sendDoubleTapRecognized:)];
    [_webView addGestureRecognizer:recognizer];
    recognizer.delegate = self;
    [recognizer setNumberOfTapsRequired:2];
}

#pragma mark WEB LOAD

-(void)loadURL:(NSString*) path{
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:path]]];
}

CGFloat onePartX = 0;
CGFloat onePartY = 0;
- (BOOL)canSendDoubleTap:(UITapGestureRecognizer*)sender{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        onePartX = self.webView.frame.size.width/3.0;
        onePartY = self.webView.frame.size.height/4.0;
    });
    
    CGPoint point = [sender locationInView:self.webView];
    BOOL x = (point.x >= onePartX) && (point.x <= onePartX*2.0);
    BOOL y = (point.y >= onePartY*3.0);
    return x && y;
}

- (void)sendDoubleTapRecognized:(UITapGestureRecognizer*)sender{
    if([self canSendDoubleTap:sender]){
        [self showSlideMenu];
    }
}

- (id) processFunctionFromJS:(NSString *) name withArgs:(NSArray*) args error:(NSError **) error{
    NSMutableArray *listElements = [[NSMutableArray alloc] init];
    
    //вернуть статус интернета
    if ([name compare:@"test_internet_connection" options:NSCaseInsensitiveSearch] == NSOrderedSame){
        [listElements addObject:[Connect internetConnected] ? @"YES" : @"NO"];
    }
    //отправить данные на сервер
    if ([name compare:@"data_send" options:NSCaseInsensitiveSearch] == NSOrderedSame){
        dispatch_async(dispatch_get_main_queue(), ^{
            NSData *data = [NSJSONSerialization dataWithJSONObject:args options:0 error:nil];
            NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSString* responseString = [[DataManager sharedManager] sendData:jsonString];
            if(responseString != nil){
                [listElements addObject:responseString];
            }
        });
    }
    
    if ([name compare:@"get_buffer_size" options:NSCaseInsensitiveSearch] == NSOrderedSame){
        NSUInteger count = [DataManager sharedManager].buffer.count;
        [listElements addObject:[NSString stringWithFormat:@"%lu",(unsigned long)count]];
    }
    
    if ([name compare:@"get_session_id" options:NSCaseInsensitiveSearch] == NSOrderedSame){
        [listElements addObject:[User sharedUser].userSessionId];
    }

    if ([name compare:@"get_user_id" options:NSCaseInsensitiveSearch] == NSOrderedSame){
        [listElements addObject:[User sharedUser].userId];
    }
    
    if ([name compare:@"get_user_fio" options:NSCaseInsensitiveSearch] == NSOrderedSame){
        [listElements addObject:[User sharedUser].userFio];
    }
    if ([name compare:@"exit" options:NSCaseInsensitiveSearch] == NSOrderedSame){
        [self toExit];
    }
    
    if ([name compare:@"open_file" options:NSCaseInsensitiveSearch] == NSOrderedSame){
        [self openFileWithPath:args];
    }
    
    if ([name compare:@"log" options:NSCaseInsensitiveSearch] == NSOrderedSame){

    }
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:listElements options:0 error:nil];
    NSString *result = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
	
    return result;
}

#pragma mark Gesture recognizer delegate

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}

#pragma mark UIDocumentInteractionController

- (void)openFileWithPath:(NSArray*) args {
    NSData *data = [NSJSONSerialization dataWithJSONObject:args options:0 error:nil];
    NSString *filePath = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSError *jsonError;
    NSDictionary *callInfo = [NSJSONSerialization
                              JSONObjectWithData:[filePath dataUsingEncoding:NSUTF8StringEncoding]
                              options:kNilOptions
                              error:&jsonError];
    filePath = [callInfo valueForKey:@"file"];
    CGRect rect = CGRectMake([[callInfo valueForKey:@"x"] floatValue], [[callInfo valueForKey:@"y"] floatValue], 0, 0);
    
    NSString* documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* presentationFolderName = [documentsDirectory stringByAppendingPathComponent:[_presentation.path lastPathComponent]];
    
    filePath = [presentationFolderName stringByAppendingPathComponent:filePath];
    filePath = [@"file://" stringByAppendingString:filePath];
    
    NSURL* pathUrl = [NSURL URLWithString:filePath];
    _documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:pathUrl];
    [_documentInteractionController setDelegate:self];
    BOOL canOpen = [_documentInteractionController presentOpenInMenuFromRect:rect inView:self.view animated:YES];
    if(canOpen == NO){
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:@"Данный тип файлов не может быть открыт на Вашем iPad" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    
}

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller {
    return self;
}

//- doc

#pragma mark - Slide menu

- (void) fillSlideMenu{
    
    NSString *previewsPath = [_presentationFolderName stringByAppendingPathComponent:@"previews"];
    
    NSFileManager* fileManager = [NSFileManager defaultManager];
    if(![fileManager fileExistsAtPath:previewsPath]){
        return;
    }
        
    NSError *error = nil;
    NSArray *pagePaths = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:previewsPath error:&error];
    NSAssert( error == nil, @"Error reading contents of chapter directory at %@: %@", previewsPath, [error localizedDescription] );
	
    if(pagePaths.count == 0 ){
        return;
    }
    
    int totalWidth = SLIDE_SPACING;
    int index = 0;
    _slideNames = [[NSMutableArray alloc] init];
	// Iterate pages
    for( NSString *pagePath in pagePaths )
    {
        [_slideNames addObject:[pagePath stringByDeletingPathExtension]];
        NSString *fullPagePath = [previewsPath stringByAppendingPathComponent:pagePath];
        UIImage *image = [UIImage imageWithContentsOfFile:fullPagePath];
        
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(totalWidth, 10, SLIDE_WIDTH, SLIDE_HEIGHT)];
        [button setTag:index];
        [button addTarget:self action:@selector(loadFromSlideMenu:) forControlEvents:UIControlEventTouchUpInside];
        
        [button setImage:image forState:UIControlStateNormal];
        [button setImage:image forState:UIControlStateSelected];
        [button setAlpha:0.7];
        button.selected = FALSE;
        
        //  button.backgroundColor = [UIColor orangeColor];
        [_slideMenuScrollView addSubview:button];
        totalWidth += SLIDE_WIDTH + SLIDE_SPACING;
        index++;
	}
    [_slideMenuScrollView setContentSize:CGSizeMake(totalWidth, _slideMenuScrollView.frame.size.height)];
}

- (void)setupSlideMenu {
    if(!_unlockGestureRecognizer ){
        _unlockGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(closeSlideMenu)];
        _currentSlideButtonIndex = 0;
        [self fillSlideMenu];
        [self closeSlideMenu];
    }
}

-(void) showSlideMenu {
    [self.slideMenuScrollView scrollRectToVisible:CGRectMake(((SLIDE_SPACING + SLIDE_WIDTH)*_currentSlideButtonIndex - self.slideMenuScrollView.frame.size.width/2 + SLIDE_WIDTH/2), self.slideMenuScrollView.frame.origin.y, self.slideMenuScrollView.frame.size.width, self.slideMenuScrollView.frame.size.height) animated:NO];
    
    CGFloat y = (_slideNames. count > 0) ? MENU_SLIDE_YPOSITION : MENU_EMPTY_SLIDE_YPOSITION;
    [self.slideMenu setFrame:CGRectMake(0, y, self.slideMenu.frame.size.width, self.slideMenu.frame.size.height)];
    [self lock:FALSE];
}

-(IBAction)loadFromSlideMenu:(UIButton *)sender{
    _currentSlideButtonIndex = sender.tag;
    NSString * slideName = [_slideNames objectAtIndex:_currentSlideButtonIndex];
    [self sendOpenSlideCommand:slideName];
    [self closeSlideMenu];
}

-(void) lock:(BOOL) state{
    [self.webView setUserInteractionEnabled:state];
    if(state){
        [self.lockButton removeGestureRecognizer:_unlockGestureRecognizer];
        [self.lockButton setFrame:CGRectMake(1024, 0, 1024, 768)];
    }
    else{
        [self.lockButton addGestureRecognizer:_unlockGestureRecognizer];
        [self.lockButton setFrame:CGRectMake(0, 0, 1024, 768)];
    }
}

- (void)closeSlideMenu {
    [self.slideMenu setFrame:CGRectMake(0, 1500, self.slideMenu.frame.size.width, self.slideMenu.frame.size.height)];
    [self lock:TRUE];
}

- (void)sendOpenSlideCommand:(NSString*)slideName{
    [_webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"xclm.nav.go('%@')", slideName]];
}
- (IBAction)closeSlideMenuButtonPressed:(id)sender {
    [self closeSlideMenu];
}

- (IBAction)closeSessionButtonPressed:(id)sender {
    [_webView stringByEvaluatingJavaScriptFromString:@"xclm.endShow()"];
    [self closeSlideMenu];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self toExit];
    });
    
}

@end
