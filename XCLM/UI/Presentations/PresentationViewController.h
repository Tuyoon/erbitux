//
//  ViewController.h
//  Hello
//
//  Created by Mac on 11.06.13.
//  Copyright (c) 2013 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Presentation.h"
#import "WebViewInterface.h"

@interface PresentationViewController : UIViewController< WebViewInterface, UIWebViewDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, retain) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UIButton *lockButton;
@property (strong, nonatomic) IBOutlet UIView *slideMenu;
@property (strong, nonatomic) IBOutlet UIScrollView *slideMenuScrollView;

- (void)setPresentation:(Presentation*)presentation;

@end
