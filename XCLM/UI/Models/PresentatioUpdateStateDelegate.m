
//  PresentatioUpdateStateDelegate.m
//  XCLM
//
//  Created by Mac on 20.05.14.
//  Copyright (c) 2014 Mac. All rights reserved.
//

#import "PresentatioUpdateStateDelegate.h"

@implementation PresentatioUpdateStateDelegate{
    RequestResult* _requestResult;
}

- (id)initWithCompletitionBlock:(UpdateCompleteBlock)completitionBlock{
    self = [super init];
    if(self){
        _completitionBlock = completitionBlock;
        _requestResult = [[RequestResult alloc] init];
    }
    return self;
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse{
    return nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    _requestResult.success = NO;
    _requestResult.errorString = [error localizedDescription];
}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
//    _requestResult.success = YES;
//    _completitionBlock(_requestResult);
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    NSMutableData * mdata = [[NSMutableData alloc] initWithData:_requestResult.data];
    [mdata appendData:data];
    _requestResult.data  = mdata;
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    _requestResult.success = YES;
    _completitionBlock(_requestResult);
}



@end
