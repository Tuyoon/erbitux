//
//  DataStateInfo.m
//  XCLM
//
//  Created by Mac on 04.03.16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import "DataStateInfo.h"

@implementation DataStateInfo

- (instancetype)initWithJsonString:(NSString *)jsonString {
    return [self initWithJsonString:jsonString sended:NO];
}

- (instancetype)initWithJsonString:(NSString *)jsonString sended:(BOOL)sended {
    self = [super init];
    if (self) {
        self.sended = sended;
        self.jsonString = jsonString;
        self.uid = [NSString stringWithFormat:@"%f", [[NSDate new] timeIntervalSince1970]];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [self init];
    if (self) {
        self.sended = [aDecoder decodeBoolForKey:@"sended"];
        self.jsonString = [aDecoder decodeObjectForKey:@"jsonString"];
        self.uid = [aDecoder decodeObjectForKey:@"uid"];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeBool:self.sended forKey:@"sended"];
    [coder encodeObject:self.jsonString forKey:@"jsonString"];
    [coder encodeObject:self.uid forKey:@"uid"];
}

@end
