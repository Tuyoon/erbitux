//
//  DataStateInfo.h
//  XCLM
//
//  Created by Mac on 04.03.16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataStateInfo : NSObject <NSCoding>

- (instancetype)initWithJsonString:(NSString *)jsonString;
- (instancetype)initWithJsonString:(NSString*)jsonString sended:(BOOL)sended;
@property (nonatomic, assign) BOOL sended;
@property (nonatomic, strong) NSString *jsonString;
@property (nonatomic, strong) NSString *uid;

@end
