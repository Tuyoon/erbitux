//
//  Synchronization.h
//  
//
//  Created by Mac on 28.11.13.
//  Copyright (c) 2013 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SynchronizationDelegate <NSObject>

@optional
- (void)synchronizationProgressChanged:(CGFloat)progress;
- (void)synchronizationComplete:(NSString*)rootPath;
- (void)synchronizationPartComplete:(NSString*)version rootPath:(NSString*)rootPath;
- (void)synchronizationFailed:(NSString*)description;
- (void)synchronizationCanceled:(NSString*)description;

@end

@interface Synchronization : NSObject<NSURLConnectionDataDelegate>

- (void)download:(NSArray*) downloadPaths;
- (void)cancel;

@property (nonatomic, strong) NSObject<SynchronizationDelegate>* delegate;

@end
