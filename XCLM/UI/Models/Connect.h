//
//  Connect.h
//  
//
//  Created by Mac on 28.11.13.
//  Copyright (c) 2013 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Synchronization.h"
#import "LoginResponse.h"
#import "PresentatioUpdateStateDelegate.h"

@class Presentation;

NSString* const kNeedInternet;

@protocol ConnectDelegate

@optional
- (void)connectLoginComplete:(LoginResponse*)loginResponse;
- (void)connectLoginFailed:(NSString*)description;
- (void)updatePresentationStateComplete:(Presentation*)presentation;
- (void)updatePresentationStateFailed:(NSString*)description;
- (void)updatePresentationComplete:(Presentation*)presentation;
- (void)updatePresentationFailed:(NSString*)description;

@end

@interface Connect : NSObject

- (void)update;
- (void)login:(NSString*)login password:(NSString*)password;
- (void)updatePresentationState:(Presentation*)presentation;
- (void)updatePresentation:(Presentation *)presentation synchronizationDelegate:(id)delegate;
- (void)cancel;
- (NSString*)sendData:(NSString*)jsonString onError:(void(^)(void))errorBlock;

+ (BOOL)internetConnected;

@property (nonatomic, strong) NSObject<ConnectDelegate>* delegate;

@end
