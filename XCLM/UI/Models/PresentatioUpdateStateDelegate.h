//
//  PresentatioUpdateStateDelegate.h
//  XCLM
//
//  Created by Mac on 20.05.14.
//  Copyright (c) 2014 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RequestResult.h"

typedef void (^UpdateCompleteBlock)(RequestResult* requestResult);

@interface PresentatioUpdateStateDelegate : NSObject<NSURLConnectionDataDelegate>

- (id)initWithCompletitionBlock:(UpdateCompleteBlock)completitionBlock;

@property (nonatomic, strong) UpdateCompleteBlock completitionBlock;

@end
