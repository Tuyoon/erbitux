//
//  Synchronization.m
//  
//
//  Created by Mac on 28.11.13.
//  Copyright (c) 2013 Mac. All rights reserved.
//

#import "Synchronization.h"
#import "ZipArchive.h"
#import "PresentationViewController.h"

@interface Synchronization (){
    NSURLConnection* _connection;
    NSString* _zipFileName;
    long _datalength;
    NSInteger _currentversion;
    NSString *_documentsDirectory;
    NSString *_zipFilePath;
    NSString *_output;

    int _indexOfLoading;
    NSArray* _downLoadPaths;
    float _progress ;
    
    NSFileHandle * _file;

}
@end

@implementation Synchronization

-(void) dealloc {
    [_connection cancel];
    _connection = nil;
}

- (void) downloadOne:(NSString*) path{
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:path]];
    request.timeoutInterval = 300;
    _connection =  [[NSURLConnection alloc] initWithRequest:request delegate:self];
//    _connection =  [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO];
//    [_connection scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
//    [_connection start];
    if(_connection){
        
    }
    else{
        if([self.delegate respondsToSelector:@selector(synchronizationFailed:)])
            [self.delegate synchronizationFailed:@"Can not set connection to zip file"];
    }
}
- (void) download:(NSArray*) paths{

    _documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    _indexOfLoading = 0;
    _downLoadPaths = paths;
    NSDictionary* allDataDict = [_downLoadPaths objectAtIndex:0];
    
    NSString* path = [allDataDict objectForKey:@"url"];
    _currentversion = [[allDataDict objectForKey:@"version"] integerValue];
    
    _datalength = 0;
    _progress = 0;
    for(NSDictionary* d in _downLoadPaths){
        _datalength += [[d objectForKey:@"size"] longValue];
    }
    _zipFileName = [path lastPathComponent];
    _zipFilePath = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:_zipFileName];
    
    NSString* presentationName = [[path stringByDeletingLastPathComponent] lastPathComponent];
    _output = [_documentsDirectory stringByAppendingPathComponent:presentationName];
    
    [UIApplication sharedApplication].idleTimerDisabled = YES;  //off device sleep
    
    [self downloadOne:path];
}

#pragma mark -
#pragma mark Connection Delegate

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse{
    return nil;
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    //    if([response expectedContentLength] > 0)
    //        datalength = [response expectedContentLength];
    [[NSFileManager defaultManager] createFileAtPath:_zipFilePath contents:nil attributes:nil];
    _file = [NSFileHandle fileHandleForUpdatingAtPath:_zipFilePath];
}
-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    if(_file){
        [_file seekToEndOfFile];
    }
    [_file writeData:data];
    
    _progress += (float) [ data length]/ (float)_datalength;
    if([self.delegate respondsToSelector:@selector(synchronizationProgressChanged:)])
        [self.delegate synchronizationProgressChanged:_progress];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    [_file closeFile];
    ZipArchive* za = [[ZipArchive alloc] init];
    NSFileManager* fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    if([za UnzipOpenFile:_zipFilePath]){
        if([za UnzipFileTo:_output overWrite:YES] != NO){
            NSFileManager* fileManager = [NSFileManager defaultManager];
            
            NSString *deleteFile = [_output stringByAppendingPathComponent:@"xclm.delete"];
            if([fileManager fileExistsAtPath:deleteFile]){
                NSString* words = [[NSString alloc] initWithContentsOfFile:deleteFile encoding:NSUTF8StringEncoding error:&error];
                
                NSArray* list = [words componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
                if(words.length){
                    for(NSString* delF in list){
                        NSString* delPath = [_output stringByAppendingString:delF];
                        if([fileManager fileExistsAtPath:delPath]){
                            BOOL success = [fileManager removeItemAtPath:delPath error:&error];
                            if (!success){
                            }
                        }
                        else{
                        }
                    }
                }
                //remove xclm.delete
                if([fileManager fileExistsAtPath:deleteFile])
                    [fileManager removeItemAtPath:deleteFile error:nil];
            }
            if([self.delegate respondsToSelector:@selector(synchronizationPartComplete:rootPath:)])
                [self.delegate synchronizationPartComplete: [NSString stringWithFormat:@"%i",_currentversion] rootPath: _output];
        }
        else{
            if([self.delegate respondsToSelector:@selector(synchronizationFailed:)])
                [self.delegate synchronizationFailed:@"Can not unpack zip file"];
        }
        [za UnzipCloseFile];
        
        //delete zip ile in cache
        if([fileManager fileExistsAtPath:_zipFilePath]){
            BOOL success = [fileManager removeItemAtPath:_zipFilePath error:&error];
            if (!success){
            }
            else {
            }
        }
        //TODO to next zip
        if(_indexOfLoading <(_downLoadPaths.count - 1)){
            _indexOfLoading++;
            NSDictionary* allDataDict = [_downLoadPaths objectAtIndex:_indexOfLoading];
            NSString* path = [allDataDict objectForKey:@"url"];
            _currentversion = [[allDataDict objectForKey:@"version"] integerValue];
            _zipFileName = [path lastPathComponent];
            _zipFilePath = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:_zipFileName];
            [self downloadOne:path];
        }
        else{
            if([self.delegate respondsToSelector:@selector(synchronizationComplete:)])
                [self.delegate synchronizationComplete:_output];
        }
        
    }
    else{
        if([self.delegate respondsToSelector:@selector(synchronizationFailed:)])
            [self.delegate synchronizationFailed:@"Can not open zip file"];
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    if([self.delegate respondsToSelector:@selector(synchronizationFailed:)])
        [self.delegate synchronizationFailed:[error localizedDescription]];
}

- (void)cancel{
    [_connection cancel];
    _connection = nil;
    if([self.delegate respondsToSelector:@selector(synchronizationCanceled:)])
    [self.delegate synchronizationCanceled:@"Cancelled"];
}

@end
