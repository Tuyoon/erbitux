//
//  Presentation.h
//  
//
//  Created by Mac on 4/23/14.
//  Copyright (c) 2014 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Connect.h"

NSString* const kPresentationIdKey;
NSString* const kPresentationIsCriticakKey;
NSString* const kPresentationPathKey;
NSString* const kPresentationNameKey;
NSString* const kPresentationDescriptionKey;
NSString* const kPresentationVersionKey;
NSString* const kPresentationLastVersionKey;
NSString* const kPresentationImagePathKey;

@protocol PresentationDelegate
@optional

- (void)presentationStartUpdate:(Presentation*)presentation;
- (void)presentationCancelUpdate:(Presentation*)presentation;

@end

@interface Presentation : NSObject<ConnectDelegate, SynchronizationDelegate>

- (id)initWithId:(NSString*)presentationId;
- (id)initWithDictionary:(NSDictionary*)dictionary;
- (NSDictionary*)presentationDictionary;
- (BOOL)needUpdate;
- (void)save;
- (BOOL)isCritical;
- (BOOL)isExist;
- (BOOL)isUpdating;
- (BOOL)update;
- (void)updateState;
- (void)cancelUpdate;
- (UIImage*)logoImage;

@property (strong, nonatomic) NSObject<ConnectDelegate>* connectDelegate;
@property (strong, nonatomic) NSObject<SynchronizationDelegate>* synchronizationDelegate;
@property (strong, nonatomic) NSObject<PresentationDelegate>* delegate;

@property (strong, nonatomic) UIImage* image;
@property (strong, nonatomic) NSString* presentationId;
@property (strong, nonatomic) NSString* critical;
@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSString* descr;
@property (strong, nonatomic) NSString* path;
@property (strong, nonatomic) NSString* version;
@property (strong, nonatomic) NSString* lastVersion;
@property (strong, nonatomic) NSString* imagePath;

@end
