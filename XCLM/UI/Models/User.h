//
//  User.h
//  XCLM
//
//  Created by Mac on 23.05.14.
//  Copyright (c) 2014 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoginResponse.h"

@interface User : NSObject

@property (nonatomic, strong) NSString* userId;
@property (nonatomic, strong) NSString* userName;
@property (nonatomic, strong) NSString* userFio;
@property (nonatomic, strong) NSString* userSessionId;
@property (nonatomic, strong) NSString* userPassword;

+ (User*)sharedUser;
+ (NSString*)lastUserName;
- (id)initWithLoginResponse:(LoginResponse*)loginResponse;
- (void)save;
- (void)load;
- (void)forgot;
- (NSString*)passwordMD5;
+ (NSString *)toMD5:(NSString*)string;

@end
