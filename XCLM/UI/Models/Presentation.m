//
//  Presentation.m
//  
//
//  Created by Mac on 4/23/14.
//  Copyright (c) 2014 Mac. All rights reserved.
//

#import "Presentation.h"
#import "Loader.h"

NSString* const kPresentationIdKey = @"presentationId";
NSString* const kPresentationIsCriticakKey = @"presentationIsCritical";
NSString* const kPresentationPathKey = @"presentationPath";
NSString* const kPresentationNameKey = @"presentationName";
NSString* const kPresentationDescriptionKey = @"presentationDescription";
NSString* const kPresentationVersionKey = @"presentationVersion";
NSString* const kPresentationLastVersionKey = @"presentationLastVersion";
NSString* const kPresentationImagePathKey = @"presentationImagePath";

@implementation Presentation{
    Connect* _connect;
    BOOL _inUpdate;
    BOOL _inUpdateState;
}

- (id)initWithId:(NSString *)presentationId{
    self = [super init];
    if(self){
        _connect = [[Connect alloc] init];
        _connect.delegate = self;
        _presentationId = presentationId;
        _name = presentationId;
        _version = @"0";
        _path = @"";
        _descr = [NSString stringWithFormat:@"Description for %@", self.name];
        _critical = @"";
        _imagePath = @"";
        
//        [self updateState];
        [self loadImage];
    }
    return self;
}

- (void)loadImage{
    
//    if(_image == nil)
    {
        NSString* documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString* imagePath = [[documentsDirectory stringByAppendingPathComponent:_presentationId] stringByAppendingPathExtension:@"png"];
        NSFileManager* fileManager = [NSFileManager defaultManager];
        if([fileManager fileExistsAtPath:imagePath]){
            _image = [UIImage imageWithContentsOfFile:imagePath];
        }
        
        if([Connect internetConnected]){
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                if(_imagePath.length > 0){
                    NSData* imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:_imagePath]];
                    if(imageData != nil){
                        [imageData writeToFile:imagePath atomically:YES];
                    }
                }
                _image = nil;
                _image = [UIImage imageWithContentsOfFile:imagePath];
            });
        }

    }
}
- (id)initWithDictionary:(NSDictionary *)dictionary{
    self = [super init];
    if(self){
        _connect = [[Connect alloc] init];
        _connect.delegate = self;
        if(dictionary){
            _presentationId = [dictionary objectForKey:kPresentationIdKey];
            _critical = [dictionary objectForKey:kPresentationIsCriticakKey];
            _name = [dictionary objectForKey:kPresentationNameKey];
            _descr = [dictionary objectForKey:kPresentationDescriptionKey];
            _path = [dictionary objectForKey:kPresentationPathKey];
            _version = [dictionary objectForKey:kPresentationVersionKey];
            _lastVersion = [dictionary objectForKey:kPresentationLastVersionKey];
            _imagePath = [dictionary objectForKey:kPresentationImagePathKey];
            
            [self loadImage];
        }
    }
    return self;
}

- (NSDictionary*)presentationDictionary{
    NSMutableDictionary* dictionary = [[NSMutableDictionary alloc] init];
    if(_presentationId)
        [dictionary setObject:_presentationId forKey:kPresentationIdKey];
    if(_critical)
        [dictionary setObject:_critical forKey:kPresentationIsCriticakKey];
    if(_name)
        [dictionary setObject:_name forKey:kPresentationNameKey];
    if(_descr)
        [dictionary setObject:_descr forKey:kPresentationDescriptionKey];
    if(_path)
        [dictionary setObject:_path forKey:kPresentationPathKey];
    if(_version)
        [dictionary setObject:_version forKey:kPresentationVersionKey];
    if(_lastVersion)
        [dictionary setObject:_lastVersion forKey:kPresentationLastVersionKey];
    if(_imagePath)
        [dictionary setObject:_imagePath forKey:kPresentationImagePathKey];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}

- (NSString *)lastVersion{
    if(_lastVersion == nil){
        return @"unknown";
    }
    return _lastVersion;
}

- (BOOL)needUpdate{
    if([self isExist] == NO){
        return [self isExistOnServer];
    }
    if([_version floatValue] == [_lastVersion floatValue])
        return NO;
    return YES;
}

- (BOOL)isCritical{
    return _critical.length > 0;
}

- (BOOL)isExist{
    if([_version floatValue] > 0)
        return YES;
    return NO;
}

- (BOOL)isExistOnServer{
    if([_lastVersion floatValue] > 0)
        return YES;
    return NO;
}

- (BOOL)isUpdating{
    return _inUpdate;
}

- (BOOL)isUpdatingState{
    return _inUpdateState;
}

- (void)save{
    if(![self needUpdate]){
        _critical = nil;
    }
    [[Loader sharedInstance] savePresentation:self];
}

- (BOOL)update{
    if(_inUpdate)
        return NO;
    if([self needUpdate] == NO)
        return NO;
    _inUpdate = YES;
    [_connect updatePresentation:self synchronizationDelegate:self];
    if([self.delegate respondsToSelector:@selector(presentationStartUpdate:)])
        [self.delegate presentationStartUpdate:self];
    return YES;
}

- (void)updateState{
    if(_inUpdateState || _inUpdate)
        return;
    _inUpdateState = YES;
    [_connect updatePresentationState:self];
}

- (void)cancelUpdate{
    if(_inUpdate == NO)
        return;
    _inUpdate = NO;
    [_connect cancel];
    if([self.delegate respondsToSelector:@selector(presentationCancelUpdate:)])
        [self.delegate presentationCancelUpdate:self];

}

- (UIImage*)logoImage{
    NSString* documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* imagePath = [[documentsDirectory stringByAppendingPathComponent:_presentationId] stringByAppendingPathExtension:@"png"];

    if(_imagePath.length > 0){
        NSData* imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:_imagePath]];
        if(imageData != nil){
            [imageData writeToFile:imagePath atomically:YES];
        }
    }
    
    UIImage* image = [UIImage imageWithContentsOfFile:imagePath];
    return image;
}

#pragma mark ConnectDelegat

-(void)updatePresentationStateComplete:(Presentation *)presentation{
    _inUpdateState = NO;
    [self save];
    if([self.connectDelegate respondsToSelector:@selector(updatePresentationStateComplete:)])
        [self.connectDelegate updatePresentationStateComplete:presentation];
}

-(void)updatePresentationComplete:(id)presentation{
    _inUpdate = NO;
    [self save];
    if([self.connectDelegate respondsToSelector:@selector(updatePresentationComplete:)])
        [self.connectDelegate updatePresentationComplete:presentation];
}
	
- (void)updatePresentationFailed:(NSString *)description{
    _inUpdate = NO;
    if([self.connectDelegate respondsToSelector:@selector(updatePresentationFailed:)])
        [self.connectDelegate updatePresentationFailed:description];
}

- (void)updatePresentationStateFailed:(NSString *)description{
    _inUpdateState = NO;
    if([self.connectDelegate respondsToSelector:@selector(updatePresentationStateFailed:)])
        [self.connectDelegate updatePresentationStateFailed:description];
}

#pragma mark SynchronizationDelegate

- (void)synchronizationPartComplete:(NSString *)version rootPath:(NSString *)rootPath{
    self.version = version;
    self.path = rootPath;
    [self save];
    if([self.synchronizationDelegate respondsToSelector:@selector(synchronizationPartComplete:rootPath:)])
       [self.synchronizationDelegate synchronizationPartComplete:version rootPath:rootPath];
}

- (void)synchronizationComplete:(NSString *)rootPath{
    _inUpdate = NO;
    self.version = self.lastVersion;
     self.path = rootPath;
    [self save];
    if([self.synchronizationDelegate respondsToSelector:@selector(synchronizationComplete:)])
        [self.synchronizationDelegate synchronizationComplete:rootPath];
}

- (void)synchronizationFailed:(NSString *)description{
    _inUpdate = NO;
    if([self.synchronizationDelegate respondsToSelector:@selector(synchronizationFailed:)])
        [self.synchronizationDelegate synchronizationFailed:description];
}

- (void)synchronizationProgressChanged:(CGFloat)progress{
    if([self.synchronizationDelegate respondsToSelector:@selector(synchronizationProgressChanged:)])
        [self.synchronizationDelegate synchronizationProgressChanged:progress];
}

@end
