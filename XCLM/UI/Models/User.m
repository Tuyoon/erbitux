//
//  User.m
//  XCLM
//
//  Created by Mac on 23.05.14.
//  Copyright (c) 2014 Mac. All rights reserved.
//

#import "User.h"
#import <CommonCrypto/CommonDigest.h>

NSString* const kSavedUserKey = @"savedName";
NSString* const kLastUserKey = @"lastUserName";

NSString* const kUserIdKey = @"userId";
NSString* const kUserNameKey = @"userName";
NSString* const kUserPasswordKey = @"userPassword";
NSString* const kUserFioKey = @"userFio";
NSString* const kUserSessionKey = @"userSession";

@implementation User{
    NSUserDefaults* _defaults;
}

static User* instance;

+ (NSString *)lastUserName{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kLastUserKey];
}

+ (User*)sharedUser{
    if(instance == nil){
        instance = [[User alloc] init];
        [instance load];
    }
    return instance;
}

- (id)init{
    self = [super init];
    if(self){
        instance = self;
        _defaults = [NSUserDefaults standardUserDefaults];
    }
    return self;
}

- (id)initWithLoginResponse:(LoginResponse*)loginResponse{
    self = [self init];
    if(self){
        _userId = loginResponse.userId;
        _userName = loginResponse.userName;
        _userPassword = loginResponse.userPassword;
        _userFio = loginResponse.userFio;
        _userSessionId = loginResponse.userId;
        [_defaults setObject:_userName forKey:kLastUserKey];
    }
    return self;
}

- (void)loadFromDictionary:(NSDictionary*)dictionary{
    _userId = [dictionary valueForKey:kUserIdKey];
    _userName = [dictionary valueForKey:kUserNameKey];
    _userPassword = [dictionary valueForKey:kUserPasswordKey];
    _userFio = [dictionary valueForKey:kUserFioKey];
    _userSessionId = [dictionary valueForKey:kUserSessionKey];
}

- (NSDictionary*)dictionaryForSave{
    NSMutableDictionary* dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setObject:_userId forKey:kUserIdKey];
    [dictionary setObject:_userName forKey:kUserNameKey];
    [dictionary setObject:_userPassword forKey:kUserPasswordKey];
    [dictionary setObject:_userFio forKey:kUserFioKey];
    [dictionary setObject:_userSessionId forKey:kUserSessionKey];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}

- (void)load{
    NSDictionary* dictionary = [_defaults objectForKey:kSavedUserKey];
    [self loadFromDictionary:dictionary];
}

- (void)save{
    NSDictionary* dictionary = [self dictionaryForSave];
    [_defaults setObject:dictionary forKey:kSavedUserKey];
    [_defaults synchronize];
}

- (void)forgot{
    [_defaults removeObjectForKey:kSavedUserKey];
    [_defaults synchronize];
}

- (NSString *)passwordMD5{
    return [User toMD5:_userPassword];
}

+ (NSString *)toMD5:(NSString*)string{
    const char *cStr = [string UTF8String];
    unsigned char digest[16];
    CC_MD5(cStr, strlen(cStr), digest);
    
    NSMutableString *resultString = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH*2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [resultString appendFormat:@"%02x", digest[i]];
    
    return resultString;

}

@end
