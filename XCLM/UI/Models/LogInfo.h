//
//  LogInfo.h
//  XCLM
//
//  Created by Mac on 20.04.15.
//  Copyright (c) 2015 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LogInfo : NSObject

@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* dateString;
@property (nonatomic, strong) NSString* path;
@property (nonatomic, strong) NSString* statsString;

@end
