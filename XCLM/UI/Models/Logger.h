//
//  Logger.h
//  XCLM
//
//  Created by Mac on 20.04.15.
//  Copyright (c) 2015 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LogInfo.h"

@interface Logger : NSObject

- (id)initWithPresentationId:(NSString*)presentationId date:(NSDate*)date;
- (void)addPositiveLineToLog:(NSString*)line;
- (void)addNegativeLineToLog:(NSString*)line;
+ (void)clear;

+ (NSArray*)allLogs;    //Array of LogInfo objects

@end
