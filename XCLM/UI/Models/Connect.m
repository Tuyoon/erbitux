//
//  Connect.m
//  
//
//  Created by Mac on 28.11.13.
//  Copyright (c) 2013 Mac. All rights reserved.
//

#import "User.h"
#import "Connect.h"
#import <CommonCrypto/CommonDigest.h>
#import "JsonParser.h"
#import "RequestResult.h"
#import "Presentation.h"
#import "Reachability.h"

@interface Connect ()<UIAlertViewDelegate>{
    Synchronization* _synchronization;
    PresentatioUpdateStateDelegate* _connectionDelegate;
    NSURLConnection* _connection;
}

@end

@implementation Connect

+ (BOOL)internetConnected{
    NetworkStatus status = [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];
    return (status == ReachableViaWiFi) || (status == ReachableViaWWAN);
}

- (void)update{
    [self checkForNewVersion];
}

- (void)cancel{
    [_connection cancel];
    [_synchronization cancel];
}
- (void)dealloc {
    [_connection cancel];
    [_synchronization cancel];
    _connection = nil;
}

-(void)checkForNewVersion{
    if(![Connect internetConnected]){
        return;
    }

    NSURL *url = [NSURL URLWithString:kVersionCheckServer];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.timeoutInterval = kTimeInterval;
    [request setHTTPMethod:@"POST"];
    [self startRequest:request block:^(RequestResult *requestResult) {
        if(requestResult.success == NO){
            return;
        }
        if(requestResult.data != nil){
            ApplicationVersionResponse* versionResponse = [JsonParser parseApplicationVersionResponse:requestResult.data];
            NSString* bundleVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];

            versionResponse.url = kApplicationServer;
            if([versionResponse.currentVersion floatValue] > [bundleVersion floatValue]){
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Доступна новая версия приложения!" message:versionResponse.url delegate:self cancelButtonTitle:@"Обновить" otherButtonTitles: nil];
                [alert show];
            }
        }

    }];
}

- (NSString*)sendData:(NSString*)jsonString onError:(void(^)(void))errorBlock{
    NSString* responseString;
    if([Connect internetConnected]){
        //отправляем данные на сервер и возвращаем странице ответ NSData
        NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"%@",kDataServer]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        NSString* postStrData = [NSString stringWithFormat:@"data=%@", jsonString];
        NSData *postData = [postStrData dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:postData];
        [request setHTTPMethod:@"POST"];

        NSError *error = nil;
        NSHTTPURLResponse *response;
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        if (response.statusCode != 200 ) {
            if (errorBlock != nil) {
                errorBlock();
            }
        }
        else if (![responseString isEqualToString:@"{\"result\": \"ok\"}"]) {
            if (errorBlock != nil) {
                errorBlock();
            }
        }
        else if(error != nil){
            if (errorBlock != nil) {
                errorBlock();
            }
        }
    }
    else{
        if(errorBlock != nil)
            errorBlock();
    }
    return responseString;
}

- (void)updatePresentation:(Presentation *)presentation synchronizationDelegate:(id)delegate{
    if(![Connect internetConnected]){
        [self.delegate updatePresentationFailed:@"No internet"];
        return;
    }
    NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"%@/%@/%@",kPresentationsServer, presentation.presentationId, presentation.version]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.timeoutInterval = kTimeInterval;
    [request setHTTPMethod:@"POST"];
    [self startRequest:request block:^(RequestResult *requestResult) {
        if(requestResult.success == NO){
            [self.delegate updatePresentationFailed:requestResult.errorString];
            return;
        }
        
        if(requestResult.data != nil){
            PresentationResponse* presentationResponse = [JsonParser parsePresentationResponse:requestResult.data];
            if(presentationResponse.parseError){
                [self.delegate updatePresentationFailed:@"json parsing error"];
                return;
            }
            presentation.lastVersion = presentationResponse.lastVersion;
            if([presentation needUpdate]){
                _synchronization = [[Synchronization alloc] init];
                _synchronization.delegate = delegate;
                [_synchronization download:presentationResponse.zipList];
            }
            else{
                [self.delegate updatePresentationComplete:presentation];
            }
        }
        else{
            [self.delegate updatePresentationComplete:presentation];
        }
    }];
}

-(void)updatePresentationState:(Presentation *)presentation{
    if(![Connect internetConnected]){
        [self.delegate updatePresentationStateFailed:@"state update no internet"];
        return;
    }
    NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"%@/%@/%@",kPresentationsServer, presentation.presentationId, presentation.version]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.timeoutInterval = kTimeInterval;
    [request setHTTPMethod:@"POST"];
    [self startRequest:request block:^(RequestResult *requestResult) {
        if(requestResult.success == NO){
            [self.delegate updatePresentationStateFailed:requestResult.errorString];
            return;
        }
        if(requestResult.data != nil){
            PresentationResponse* presentationResponse = [JsonParser parsePresentationResponse:requestResult.data];
            if(presentationResponse.parseError){
                [self.delegate updatePresentationStateFailed:@"json parsing error"];
                return;
            }
            presentation.lastVersion = presentationResponse.lastVersion;
        }
        if([self.delegate respondsToSelector:@selector(updatePresentationStateComplete:)])
            [self.delegate updatePresentationStateComplete:presentation];
    }];
}

- (void)login:(NSString*)login password:(NSString*)password{
    if(![Connect internetConnected]){
        [self.delegate connectLoginFailed:kNeedInternet];
        return;
    }
    NSString* data = [NSString stringWithFormat:@"%@-%@", login, password];
    NSData *postData = [data dataUsingEncoding:[NSString defaultCStringEncoding]];
    NSString* appversion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSString* loginString = [NSString stringWithFormat:kLoginServer, login, [User toMD5:password], appversion];
    NSURL *url = [NSURL URLWithString:loginString];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.timeoutInterval = kTimeInterval;
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    [self startRequest:request block:^(RequestResult *requestResult) {
        if(requestResult.success == NO){
            [self.delegate connectLoginFailed:requestResult.errorString];
            return;
        }
        if(requestResult.data != nil){
            LoginResponse* loginResponse = [JsonParser parseLoginResponse:requestResult.data];
            if(loginResponse.parseError){
                [self.delegate connectLoginFailed:@"json parsing error"];
                return;
            }
            if(loginResponse.success == NO){
                [self.delegate connectLoginFailed:loginResponse.errorString];
                return;
            }
            NSString* bundleVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
            if([loginResponse.applicationVersion floatValue] > [bundleVersion floatValue]){
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Доступна новая версия приложения!" message:loginResponse.applicationUrl delegate:self cancelButtonTitle:@"Обновить" otherButtonTitles: nil];
                [alert show];
            }
            loginResponse.userName = login;
            loginResponse.userPassword = password;
            [self.delegate connectLoginComplete:loginResponse];
        }
        else{
            [self.delegate connectLoginFailed:@"not response"];
        }
    }];
}

- (void)startRequest:(NSURLRequest*)request block:(UpdateCompleteBlock)block{
    _connectionDelegate = [[PresentatioUpdateStateDelegate alloc] initWithCompletitionBlock:block];
    _connection = [[NSURLConnection alloc] initWithRequest:request delegate:_connectionDelegate];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:alertView.message]];
}

@end
