//
//  Logger.m
//  XCLM
//
//  Created by Mac on 20.04.15.
//  Copyright (c) 2015 Mac. All rights reserved.
//

#import "Logger.h"

@implementation Logger {
    
    NSString* _logBody;
    NSString* _logPath;
    NSUInteger _count;
    NSUInteger _positiveCount;
}
static NSString* logsPath;
static NSDateFormatter* dateFormatter;

- (id)initWithPresentationId:(NSString*)presentationId date:(NSDate*)date {
    self = [super init];
    if(self){
        [Logger createLogPath];
        [Logger createLogDirectory];
        [Logger createDateFormatter];
        [self startLogForPresentationId:presentationId date:date];
    }
    return self;
}

+ (void)createDateFormatter{
    if(!dateFormatter){
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-YYYY HH:mm:ss"];
//        [dateFormatter setDateStyle:NSDateFormatterShortStyle];
//        [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
    }
}

+ (void)createLogPath {
    if(logsPath.length == 0){
        NSString* documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        logsPath = [documentsDirectory stringByAppendingPathComponent:@"Log"];
    }
}

+ (void)createLogDirectory {
    if (![[NSFileManager defaultManager] fileExistsAtPath:logsPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:logsPath withIntermediateDirectories:NO attributes:nil error:nil];
}

+ (void)removeLogDirectory{
    NSError* error;
    if ([[NSFileManager defaultManager] fileExistsAtPath:logsPath])
        [[NSFileManager defaultManager] removeItemAtPath:logsPath error:&error];
}

- (void)startLogForPresentationId:(NSString*)presentationId date:(NSDate*)date {
    _logPath = [[logsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@-%@", presentationId, [dateFormatter stringFromDate:date]] ] stringByAppendingPathExtension:@"log"];
    NSString *dateString = [dateFormatter stringFromDate:date];
    NSString *nameString = [NSString stringWithFormat:@"name: %@", presentationId];
    _logBody = @"";
    [self removeIfExist];
    [self save];
    [self addLineToLog:dateString];
    [self addLineToLog:nameString];
    [self updateStatsString];
}

- (void)addLineToLog:(NSString*)line{
    _logBody = [_logBody stringByAppendingFormat:@"\n%@", line];
    [self save];
}

- (void)removeIfExist {
    if([[NSFileManager defaultManager] fileExistsAtPath:_logPath]) {
        [[NSFileManager defaultManager] removeItemAtPath:_logPath error:nil];
    }
}

- (void)save {
    NSError *error;
    BOOL ok = [_logBody writeToFile:_logPath atomically:YES encoding:NSUTF8StringEncoding error:&error];
    if(!ok){
//        NSLog(@"Error writing file at %@\n%@", _logPath, [error localizedFailureReason]);
    }
}

- (void)addPositiveLineToLog:(NSString *)line {
    NSString *pline = [NSString stringWithFormat:@"+ %@", line];
    _positiveCount++;
    _count++;
    [self addLineToLog:pline];
    [self updateStatsString];
}

- (void)addNegativeLineToLog:(NSString *)line {
    NSString *nline = [NSString stringWithFormat:@"- %@", line];
    _count++;
    [self addLineToLog:nline];
    [self updateStatsString];
}

- (void)updateStatsString {
    NSUInteger index = 0;
    NSError *error;
    NSString *fileContents = [NSString stringWithContentsOfFile:_logPath encoding:NSUTF8StringEncoding error:&error];
    NSArray *lines = [fileContents componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    for (NSString *line in lines) {
        index = line.length;
        break;
    }
    NSString *statsString = [NSString stringWithFormat:@"%d/%d", _positiveCount, _count];
    _logBody = [statsString stringByAppendingString:[_logBody substringFromIndex:index]];
    [self save];
}

+ (void)clear{
    [Logger createLogPath];
    [Logger removeLogDirectory];
    [Logger createLogDirectory];
}

+ (NSArray*)allLogs {
    [Logger createLogPath];
    [Logger createLogDirectory];
    [Logger createDateFormatter];
    NSMutableArray* logs = [[NSMutableArray alloc] init];
    NSError *error = nil;
    NSArray *logPaths = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:logsPath error:&error];
    for (NSString* logPath in logPaths) {
        LogInfo* logInfo = [[LogInfo alloc] init];
        logInfo.name = [logPath lastPathComponent];
        logInfo.path = [logsPath stringByAppendingPathComponent: [logPath lastPathComponent]] ;
        
        // read first line to stat string
        NSString *fileContents = [NSString stringWithContentsOfFile:logInfo.path encoding:NSUTF8StringEncoding error:NULL];
        int i = 0;
        NSArray *lines = [fileContents componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
        for (NSString *line in lines) {
            if (i == 0) {
                logInfo.statsString = line;
            }
            else if (i == 1) {
                logInfo.dateString = line;
            }
            else {
                break;
            }
            i++;
        }
        
        [logs addObject:logInfo];
    }
    
    return [NSArray arrayWithArray:logs];
}


@end
