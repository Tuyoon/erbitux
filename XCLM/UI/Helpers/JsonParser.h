//
//  JsonParser.h
//  
//
//  Created by Mac on 09.05.14.
//  Copyright (c) 2014 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoginResponse.h"
#import "PresentationResponse.h"
#import "ApplicationVersionResponse.h"

@interface JsonParser : NSObject

+ (LoginResponse*)parseLoginResponse:(NSData*)responseData;
+ (PresentationResponse*)parsePresentationResponse:(NSData*)responseData;
+ (ApplicationVersionResponse *)parseApplicationVersionResponse:(NSData *)responseData;

@end
