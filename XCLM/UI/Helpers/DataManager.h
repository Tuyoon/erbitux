//
//  DataManager.h
//  XCLM
//
//  Created by Mac on 03.03.16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Presentation.h"

@interface DataManager : NSObject

@property (nonatomic, strong, readonly) NSMutableDictionary *allData;
@property (nonatomic, strong, readonly) NSMutableDictionary *buffer;

+ (instancetype) sharedManager;
- (void)startSessionForPresentation:(Presentation*)presentation;
- (void)stopCurrentSession;
- (NSString*)sendData:(NSString*)jsonString;

@end
