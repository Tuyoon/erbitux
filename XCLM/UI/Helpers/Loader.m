//
//  Loader.m
//  
//
//  Created by Mac on 4/23/14.
//  Copyright (c) 2014 Mac. All rights reserved.
//

#import "Loader.h"

NSString* const kPresentationsKey = @"presentations";
NSString* const kLastPresentationsKey = @"lastPresentations";



static Loader* _instance;
@implementation Loader{
    NSUserDefaults* _defaults;
}

+ (id)sharedInstance{
    if(!_instance){
        _instance = [[Loader alloc] init];
        [_instance start];
        
    }
    return _instance;
}

- (void)start{
    _defaults = [NSUserDefaults standardUserDefaults];
}

- (NSArray *)loadLast{
    NSArray* presentationsId = [_defaults objectForKey:kLastPresentationsKey];
    NSMutableArray* presentations = [[NSMutableArray alloc] init];
    NSDictionary* _presentations = [_defaults objectForKey:kPresentationsKey];
    for(NSString* presentationId in presentationsId){
        Presentation* presentation = [[Presentation alloc] initWithDictionary:[_presentations objectForKey:presentationId]];
        [presentations addObject:presentation];
    }
    return [NSArray arrayWithArray:presentations];
}

- (NSArray*)load:(NSArray*)presentationsId{
    NSMutableArray* presentations = [[NSMutableArray alloc] init];
    NSDictionary* _presentations = [_defaults objectForKey:kPresentationsKey];
    if(_presentations == nil){
        _presentations = [[NSDictionary alloc] init];
    }
    for(NSDictionary* presentationInfo in presentationsId){
        NSString* presentationId = [presentationInfo valueForKey:@"sysname"];
        NSDictionary* dictionary = [_presentations objectForKey:presentationId];
        Presentation* presentation;
        if(dictionary){
            presentation = [[Presentation alloc] initWithDictionary:dictionary];
        }
        else{
            presentation = [[Presentation alloc] initWithId:presentationId];
        }
        presentation.imagePath = [presentationInfo valueForKey:@"logo"];
        presentation.name = [presentationInfo valueForKey:@"title"];
        presentation.lastVersion = [presentationInfo valueForKey:@"version"];
        [presentations addObject:presentation];
    }
    
    return [NSArray arrayWithArray:presentations];
}

- (void)save:(NSArray*)presentations{
    NSMutableDictionary* _presentations = [NSMutableDictionary dictionaryWithDictionary:[_defaults objectForKey:kPresentationsKey]];
    NSMutableArray* lastPresentations = [[NSMutableArray alloc] init];
    if(_presentations == nil)
        _presentations = [[NSMutableDictionary alloc] init];
    for(Presentation* presentation in presentations){
        [_presentations setObject:presentation.presentationDictionary forKey:presentation.presentationId];
        [lastPresentations addObject:presentation.presentationId];
    }
    [self saveLast:lastPresentations];
    [_defaults setObject:_presentations forKey:kPresentationsKey];
    [_defaults synchronize];
}

// save last presentations
- (void)saveLast:(NSArray *)presentations{
    [_defaults setObject:presentations forKey:kLastPresentationsKey];
    [_defaults synchronize];
}


- (void)savePresentation:(Presentation*)presentation{
    NSMutableDictionary* _presentations = [NSMutableDictionary dictionaryWithDictionary:[_defaults objectForKey:kPresentationsKey]];
    [_presentations setObject:[presentation presentationDictionary] forKey:presentation.presentationId];
    [_defaults setObject:_presentations forKey:kPresentationsKey];
    [_defaults synchronize];
}

@end
