//
//  JsonParser.m
//  
//
//  Created by Mac on 09.05.14.
//  Copyright (c) 2014 Mac. All rights reserved.
//

#import "JsonParser.h"
#import "Presentation.h"


@implementation JsonParser

+ (LoginResponse*)parseLoginResponse:(NSData*)responseData{
    LoginResponse* response = [[LoginResponse alloc] init];
    NSError *jsonParsingError = nil;
    id jsondata = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&jsonParsingError];
    
    if(jsonParsingError != nil){
        response.parseError = YES;
    }
    else{
        response.success = [[jsondata valueForKey:@"result"] integerValue] > 0 ? YES: NO;
        response.errorString = [jsondata valueForKey:@"error"];
        response.presentationsIds = [jsondata objectForKey:@"presentations"];
        response.userId = [jsondata objectForKey:@"user_id"];
        response.userFio = [jsondata objectForKey:@"fio"];
        response.applicationVersion = [jsondata objectForKey:@"app_version"];
        response.applicationUrl = [jsondata objectForKey:@"app_url"];
    }
    return response;
}

+ (PresentationResponse *)parsePresentationResponse:(NSData *)responseData{
    PresentationResponse* response = [[PresentationResponse alloc] init];
    
    NSError *jsonParsingError = nil;
    id jsondata = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&jsonParsingError];
    if(jsonParsingError != nil){
        response.parseError = YES;
    }
    response.zipList = [jsondata objectForKey:@"ziplist"];
    response.lastVersion = [jsondata objectForKey:@"current_version"];
    return response;
}

+ (ApplicationVersionResponse *)parseApplicationVersionResponse:(NSData *)responseData{
    ApplicationVersionResponse* response = [[ApplicationVersionResponse alloc] init];
    
    NSError *jsonParsingError = nil;
    id jsondata = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&jsonParsingError];
    if(jsonParsingError != nil){
        response.parseError = YES;
    }
    response.currentVersion = [jsondata objectForKey:@"current_version"];
    response.url = [jsondata objectForKey:@"url"];
    return response;
}

@end
