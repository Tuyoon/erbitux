//
//  DataManager.m
//  XCLM
//
//  Created by Mac on 03.03.16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import "DataManager.h"
#import "Connect.h"
#import "Reachability.h"
#import "Logger.h"
#import "DataStateInfo.h"

NSString* const kAllDataKey = @"allDataKey";
NSString* const kBufferKey = @"bufferKey";
NSString* const kBufferPresentationsKey = @"bufferPresentationsKey";


@interface DataManager ()

@property (nonatomic, strong, readwrite) NSMutableDictionary *allData;
@property (nonatomic, strong, readwrite) NSMutableDictionary *buffer;
@property (nonatomic, strong, readwrite) NSMutableDictionary *bufferPresentations;

@property (nonatomic, strong) Reachability *internetReachability;
@property (nonatomic, strong) NSUserDefaults *userdefs;
@property (nonatomic, strong) Connect *connect;
@property (nonatomic, strong) Logger *logger;

@property (nonatomic, strong) NSString *currentSession;
@property (nonatomic, assign) BOOL sending;

@end

@implementation DataManager

+ (instancetype)sharedManager {
    static DataManager *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[DataManager alloc] init];
    });
    
    return instance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.connect = [[Connect alloc] init];
        self.userdefs  = [NSUserDefaults standardUserDefaults];
        [self loadSavedData];
        [self initReachability];
    }
    
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)startSessionForPresentation:(Presentation*)presentation {
    NSDate *date = [NSDate new];
    self.currentSession = [NSString stringWithFormat:@"%f", date.timeIntervalSince1970];
    self.bufferPresentations[self.currentSession] = presentation.presentationId;
    self.logger = [[Logger alloc] initWithPresentationId:presentation.presentationId date:date];
}

- (void)stopCurrentSession {
    self.currentSession = nil;
    self.logger = nil;
}

- (NSString*)sendData:(NSString*)jsonString {
    BOOL error = NO;
    DataStateInfo *stateInfo = [[DataStateInfo alloc] initWithJsonString:jsonString];
    NSString *sendResult = [self sendData:jsonString session:self.currentSession error:&error];
    if (error) {
        if (!self.buffer[self.currentSession]) {
            self.buffer[self.currentSession] = [[NSMutableArray alloc] init];
        }
        [self.buffer[self.currentSession] addObject:stateInfo.uid];
        [self.logger addNegativeLineToLog:jsonString];
    }
    else {
        stateInfo.sended = YES;
        [self.logger addPositiveLineToLog:jsonString];
    }
    [self saveToSendDataForSession:self.currentSession];

    if (!self.allData[self.currentSession]) {
        self.allData[self.currentSession] = [[NSMutableArray alloc] init];
    }
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:stateInfo];
    [self.allData[self.currentSession] addObject:encodedObject];
    [self saveAllData];
    
    return sendResult;
}

- (void)sendData {
    if (self.sending) {
        return;
    }
    self.sending = YES;
    for (NSString *session in self.buffer.allKeys) {
        NSArray* sessionData = [NSArray arrayWithArray:self.buffer[session]];
        NSString *presentationId = self.bufferPresentations[session];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:[session doubleValue]];
        Logger *logger = [[Logger alloc] initWithPresentationId:presentationId date:date];
        
        for (NSData *data in self.allData[session]) {
            DataStateInfo* stateInfo = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            BOOL infoNeedToSend = NO;
            for (NSString *session in sessionData) {
                if ([session rangeOfString:stateInfo.uid].location != NSNotFound) {
                    infoNeedToSend = YES;
                    break;
                }
            }
            if (infoNeedToSend) {
                BOOL error = NO;
                [self sendData:stateInfo.jsonString session:session error:&error];
                if (error) {
                    [logger addNegativeLineToLog:stateInfo.jsonString];
                }
                else {
                    [self.buffer removeObjectForKey:session];
                    [self.bufferPresentations removeObjectForKey:session];
                    stateInfo.sended = YES;
                    [logger addPositiveLineToLog:stateInfo.jsonString];
                }
                [self saveToSendDataForSession:session];

            }
            else {
                [logger addPositiveLineToLog:stateInfo.jsonString];                
            }
        }
    }
    [self saveAllData];
    self.sending = NO;
}

- (NSString*)sendData:(NSString*)jsonString session:(NSString*)session error:(BOOL*)outerror{
    __block BOOL error = NO;
    NSString* responseString = [self.connect sendData:jsonString onError:^{
        error = YES;
    }];
    *outerror = error;
    
    return responseString;
}

- (void)saveAllData {
    [self.userdefs setObject:self.allData forKey:kAllDataKey];
    [self.userdefs synchronize];
}

- (void)saveToSendData {
    for (NSString *session in self.buffer.allKeys) {
        [self saveToSendDataForSession:session];
    }
}

- (void)saveToSendDataForSession:(NSString*)session {
    NSMutableDictionary* sessionsDictionary = [NSMutableDictionary dictionaryWithDictionary:[self.userdefs objectForKey:kBufferKey]];
    sessionsDictionary[session] = self.buffer[session];
    [self.userdefs setObject:sessionsDictionary forKey:kBufferKey];

    NSMutableDictionary* sessionPresentationsDictionary = [NSMutableDictionary dictionaryWithDictionary:[self.userdefs objectForKey:kBufferPresentationsKey]];
    sessionPresentationsDictionary[session] = self.bufferPresentations[session];
    [self.userdefs setObject:sessionPresentationsDictionary forKey:kBufferPresentationsKey];

    [self.userdefs synchronize];
}

- (void)loadSavedData {
    NSDictionary* dictionary = [self.userdefs objectForKey:kAllDataKey];
    self.allData = [NSMutableDictionary dictionaryWithDictionary:dictionary];
    dictionary = [self.userdefs objectForKey:kBufferKey];
    self.buffer = [NSMutableDictionary dictionaryWithDictionary:dictionary];
    dictionary = [self.userdefs objectForKey:kBufferPresentationsKey];
    self.bufferPresentations = [NSMutableDictionary dictionaryWithDictionary:dictionary];
}

#pragma mark -
#pragma mark Reachability

- (void)initReachability{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
    self.internetReachability = [Reachability reachabilityForInternetConnection];
    [self.internetReachability startNotifier];
    [self tryToSendData];
}

- (void) reachabilityChanged:(NSNotification *)note{
    Reachability* reachability = [note object];
    NSParameterAssert([reachability isKindOfClass:[Reachability class]]);
    [self tryToSendData];
}

- (void) tryToSendData{
    if([Connect internetConnected]){
        [self sendData];
        [self saveToSendData];
    }
}

@end
