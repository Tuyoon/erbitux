//
//  Loader.h
//  
//
//  Created by Mac on 4/23/14.
//  Copyright (c) 2014 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Presentation.h"


@interface Loader : NSObject

+ (id)sharedInstance;
- (NSArray*)load:(NSArray*)presentationsId;
- (NSArray*)loadLast;
- (void)save:(NSArray*)presentations;
- (void)savePresentation:(Presentation*)presentation;

@end
