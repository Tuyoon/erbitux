#import <UIKit/UIKit.h>

@interface LoadingView : UIView

@property (nonatomic, readonly) UILabel* label;
@property (nonatomic, readonly) UIActivityIndicatorView* activityIndicator;

+ (LoadingView*)loadingViewForView:(UIView *)superView;
+ (LoadingView*)loadingViewForView:(UIView*)superView withText:(NSString*)text;

@end
