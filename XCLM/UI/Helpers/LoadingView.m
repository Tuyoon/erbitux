
#import "LoadingView.h"

@interface LoadingView () {
	UIView* _visibleView;
}

@end;

@implementation LoadingView

static CGFloat margin = 20;

+ (LoadingView*)loadingViewForView:(UIView *)superView {
	return [LoadingView loadingViewForView:superView withText:@"Loading"];
}

+ (LoadingView *)loadingViewForView:(UIView *)superView withText:(NSString *)text {
	LoadingView* view = [[LoadingView alloc]initWithFrame:superView.bounds];
	[superView addSubview:view];
	view.label.text = text;
	[view setNeedsLayout];
	return view;
}

- (id)initWithFrame:(CGRect)frame {
	if (self = [super initWithFrame:frame]) {
		self.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
		self.userInteractionEnabled = YES;
		self.exclusiveTouch = YES;
		self.backgroundColor = [UIColor clearColor];
		_visibleView = [[UIView alloc] initWithFrame:CGRectZero];
		_visibleView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.75];
		_visibleView.layer.borderColor = [[UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1] CGColor];
		_visibleView.layer.borderWidth = 2;
		_visibleView.layer.cornerRadius = 15;
		
		_activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
		[_visibleView addSubview:_activityIndicator];
		[_activityIndicator startAnimating];
		
		_label = [[UILabel alloc]init];
        _label.backgroundColor = [UIColor clearColor];
		_label.lineBreakMode = NSLineBreakByWordWrapping;
		_label.textAlignment = NSTextAlignmentCenter;
		_label.textColor = [UIColor whiteColor];
		[_visibleView addSubview:_label];
		
		[self addSubview:_visibleView];
	}
	return self;
}

- (void)layoutSubviews {
	CGFloat maxHeight = self.bounds.size.height - 5 * margin - _activityIndicator.bounds.size.height;
	NSAttributedString* attributedText = [[NSAttributedString alloc]initWithString:_label.text attributes:@{NSFontAttributeName: _label.font}];
	CGSize textSize = [attributedText boundingRectWithSize:CGSizeMake(self.bounds.size.width - 4 * margin, maxHeight) options:NSStringDrawingUsesLineFragmentOrigin context:nil].size;
	CGFloat width = MAX(_activityIndicator.bounds.size.width, textSize.width) + margin * 2;
	if (width < self.bounds.size.width / 2) {
		width = self.bounds.size.width / 2;
	}
	_visibleView.bounds = CGRectMake(0, 0, width, _activityIndicator.bounds.size.height + textSize.height + margin * 3);
	_activityIndicator.frame = CGRectMake((width - _activityIndicator.bounds.size.width) / 2, margin, _activityIndicator.bounds.size.width, _activityIndicator.bounds.size.height);
	_label.frame = CGRectMake((width - textSize.width) / 2, margin + CGRectGetMaxY(_activityIndicator.frame), textSize.width, textSize.height);
	_visibleView.center = self.center;
}

@end
