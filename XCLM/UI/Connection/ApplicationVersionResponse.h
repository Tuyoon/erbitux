//
//  ApplicationVersionResponse.h
//  XCLM
//
//  Created by Mac on 24.05.14.
//  Copyright (c) 2014 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ApplicationVersionResponse : NSObject

@property (nonatomic) BOOL parseError;
@property (nonatomic, strong) NSString* currentVersion;
@property (nonatomic, strong) NSString* url;

@end
