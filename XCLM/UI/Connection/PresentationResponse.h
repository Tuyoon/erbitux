//
//  PresentationResponse.h
//  CLMLogin
//
//  Created by Mac on 10.05.14.
//  Copyright (c) 2014 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PresentationResponse : NSObject

@property (nonatomic) BOOL parseError;
@property (nonatomic, strong) NSString* lastVersion;
@property (nonatomic, strong) NSString* version;
@property (nonatomic, strong) NSArray* zipList;

@end
