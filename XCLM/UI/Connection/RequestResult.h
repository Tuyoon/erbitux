//
//  RequestResult.h
//  CLMLogin
//
//  Created by Mac on 10.05.14.
//  Copyright (c) 2014 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RequestResult : NSObject

@property (nonatomic) BOOL success;
@property (nonatomic, strong) NSData* data;
@property (nonatomic) NSString* errorString;

@end
