//
//  LoginResponse.h
//  CLMLogin
//
//  Created by Mac on 09.05.14.
//  Copyright (c) 2014 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoginResponse : NSObject

@property (nonatomic) BOOL parseError;
@property (nonatomic) BOOL success;
@property (nonatomic, strong) NSString* errorString;
@property (nonatomic, strong) NSArray* presentationsIds;
@property (nonatomic, strong) NSString* userId;
@property (nonatomic, strong) NSString* userName;
@property (nonatomic, strong) NSString* userPassword;
@property (nonatomic, strong) NSString* userFio;
@property (nonatomic, strong) NSString* applicationVersion;
@property (nonatomic, strong) NSString* applicationUrl;


@end
