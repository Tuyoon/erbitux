//
//  AppDelegate.h
//  Hello
//
//  Created by Mac on 11.06.13.
//  Copyright (c) 2013 Mac. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end